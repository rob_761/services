import {Component, OnInit} from '@angular/core';
import {AccountsService} from "./accounts.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
 accounts: {name: string, status: string} [] = []; //initialize to empty array

 constructor(private accountsService: AccountsService) {}

 ngOnInit() {
   this.accounts = this.accountsService.accounts; //passing reference
 }
}
